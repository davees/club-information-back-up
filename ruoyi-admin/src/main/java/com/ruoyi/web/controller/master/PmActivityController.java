package com.ruoyi.web.controller.master;

import java.text.ParseException;
import java.util.*;

import com.ruoyi.approval.mapper.ApprovalMapper;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.approval.result.ApprovalResultLog;
import com.ruoyi.common.utils.ExceptionFinalMessage;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.master.service.IPmActivityService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动主Controller
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/master/activity")
public class PmActivityController extends BaseController {

	@Autowired
	private IPmActivityService iPmActivityService;


	@Autowired
	private ApprovalMapper approvalMapper;

	//搜索参数列表
	private HashMap<String, Object> searchParam = new HashMap<>(10);

	/**
	 * 查询活动主列表
	 */
	@PreAuthorize("@ss.hasPermi('master:activity:list')")
	@GetMapping("/list")
	public TableDataInfo list(@RequestParam(value = "title", required = false) String title,
							  @RequestParam(value = "params[endTime]", required = false) String endDate,
							  @RequestParam(value = "params[beginTime]", required = false) String beginDate,
							  @RequestParam(value = "approvalStatus", required = false) String approvalStatus,
							  @RequestParam(value = "place", required = false) String place) {
		startPage();
		searchParam.put("title", title);
		searchParam.put("endDate", endDate);
		searchParam.put("beginDate", beginDate);
		searchParam.put("approvalStatus", approvalStatus);
		searchParam.put("place", place);
		//重新组装页面的数据
		return getDataTable(iPmActivityService.queryList(searchParam));
	}

	/**
	 * 导出活动主列表
	 */
//    @PreAuthorize("@ss.hasPermi('master:activity:export')" )
//    @Log(title = "活动主" , businessType = BusinessType.EXPORT)
//    @GetMapping("/export" )
//    public AjaxResult export() {
//        List<HashMap> list = iPmActivityService.queryList(searchParam);
//        ExcelUtil<HashMap> util = new ExcelUtil<HashMap>(HashMap.class);
//        return util.exportExcel(list, "activity" );
//    }

	/**
	 * 审批该活动
	 */
	@GetMapping(value = "/{id}")
	public AjaxResult approvalActivity(@PathVariable("id") Integer id) throws ParseException {
		//根据审批返回的结果对象进行结果的判断
		List<ApprovalResult> approvalResults = iPmActivityService.approvalActivity(id);
		for (int i = 0; i < approvalResults.size(); i++) {
			if (approvalResults.get(i).getCode().equals("10")) {
				return AjaxResult.error("提交审批失败");
			}
		}

		return AjaxResult.success();
	}


	//批量审批活动 todo修改为线程池方式
	@RequestMapping(value = "/batchApproval/{ids}")
	public AjaxResult approvalBatchActivity(@PathVariable("ids") Integer[] ids) throws Exception {

		for (Integer id : ids) {
			List<ApprovalResult> approvalResults = iPmActivityService.approvalActivity(id);
		}
		return AjaxResult.success();
	}


	@GetMapping(value = "/logList/{id}")
	public AjaxResult approvalActivityLog(@PathVariable("id") String id) throws ParseException {

		System.out.println(id);
		List<ApprovalResultLog> logs = approvalMapper.queryApprovalLogList("ACT_" + id);
		return AjaxResult.success("操作成功", logs);
	}
}

