package com.ruoyi.web.controller.api;

import com.ruoyi.api.dto.ActivityDetailDto;
import com.ruoyi.api.dto.ActivityDto;
import com.ruoyi.api.service.IMiniActivityService;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.master.domain.PmActivity;
import io.jsonwebtoken.lang.Collections;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/activity" )
public class MiniActivityController extends BaseController {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private IMiniActivityService miniActivityService;

    @Autowired
    private RedisCache redisCache;

    /**
     * 获取关注社团信息
     */
    @GetMapping(value = "/{id}" )
    @ResponseBody
    public AjaxResult getActivityInfo(@PathVariable("id" ) Long userId) {
        // 查询所有关注活动
        List<ActivityDto> activitys = miniActivityService.queryActivityList(userId);
        List<ActivityDto> finishActivitys = new ArrayList<>();
        // 在关注的活动中筛选出已结束的活动
        if (!Collections.isEmpty(activitys)) {
            activitys.forEach(activityDto -> {
                if (activityDto.getEndDate().after(new Date())) {
                    finishActivitys.add(activityDto);
                }
            });
        }
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("finishActivitys", finishActivitys);
        ajaxResult.put("activitys", activitys);
        return AjaxResult.success(ajaxResult);
    }

    /**
     * 根据活动ID获取活动详情
     */
    @GetMapping(value = "/activityDetail" )
    @ResponseBody
    public AjaxResult getActivityDetailInfo(Long activityId, Long userId) {
        ActivityDetailDto activityDetailDto = miniActivityService.queryActivityDetailList(activityId, userId);
        try {
            this.amqpTemplate.convertAndSend("exchange.activity","a.b", activityId);
        } catch (Exception e) {
            logger.error("RabbitMq异常", e);
            return AjaxResult.success(activityDetailDto);
        }
        return AjaxResult.success(activityDetailDto);
    }

	//根据用户id和活动id，完成关联
	@GetMapping(value = "/follow")
	@ResponseBody
	public AjaxResult follow(@RequestParam("userId") Long userId,
							 @RequestParam("activityId") Long activityId,
							 @RequestParam("operation") Boolean operation) {
		Integer num = miniActivityService.doFollow(userId, activityId, operation);
		if (num ==  1) {
			return AjaxResult.success();
		} else {
			return AjaxResult.error("关注设置异常");
		}
	}

	//获取热榜
	@GetMapping(value = "/getMaxTimeActivity" )
	@ResponseBody
	public AjaxResult getMaxTimeActivityList() {
        try {
            List<PmActivity> cacheList = redisCache.getCacheList(Constants.HOT_ACTIVITY_KEY);
            if (cacheList.isEmpty()) {
                throw new RuntimeException();
            }
            return AjaxResult.success(redisCache.getCacheList(Constants.HOT_ACTIVITY_KEY));
        } catch(Exception e) {
            logger.error(e.getMessage(), e);
            List<ActivityDto> activityDtos = miniActivityService.maxTimesActivityList();
            redisCache.setCacheList(Constants.HOT_ACTIVITY_KEY, activityDtos);
            return AjaxResult.success(activityDtos);
        }
	}

    //获取关注
    @GetMapping(value = "getMyActivitys/{id}" )
    @ResponseBody
    public AjaxResult getMyActivitys(@PathVariable("id" ) Long userId) {
        List<ActivityDto> activityDtos = miniActivityService.getClubActivitys(userId);
        return AjaxResult.success(activityDtos);
    }

}
