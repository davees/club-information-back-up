package com.ruoyi.web.controller.master;

import java.util.List;
import java.util.Arrays;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.redis.RedisCache;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.master.domain.PmBusiness;
import com.ruoyi.master.service.IPmBusinessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 赞助商主Controller
 *
 * @author ruoyi
 * @date 2021-03-26
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/master/business" )
public class PmBusinessController extends BaseController {

    @Autowired
    private RedisCache redisCache;

    private final IPmBusinessService iPmBusinessService;

    /**
     * 查询赞助商主列表
     */
    // @PreAuthorize("@ss.hasPermi('master:business:list')")
    @GetMapping("/list")
    public TableDataInfo list(PmBusiness pmBusiness) {
        startPage();
        List<PmBusiness> list = iPmBusinessService.queryList(pmBusiness);
        return getDataTable(list);
    }

    /**
     * 导出赞助商主列表
     */
    // @PreAuthorize("@ss.hasPermi('master:business:export')" )
    @Log(title = "赞助商主" , businessType = BusinessType.EXPORT)
    @GetMapping("/export" )
    public AjaxResult export(PmBusiness pmBusiness) {
        List<PmBusiness> list = iPmBusinessService.queryList(pmBusiness);
        ExcelUtil<PmBusiness> util = new ExcelUtil<PmBusiness>(PmBusiness.class);
        return util.exportExcel(list, "business" );
    }

    /**
     * 获取赞助商主详细信息
     */
    // @PreAuthorize("@ss.hasPermi('master:business:query')" )
    @GetMapping(value = "/{id}" )
    public AjaxResult getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(iPmBusinessService.getById(id));
    }

    /**
     * 新增赞助商主
     */
    // @PreAuthorize("@ss.hasPermi('master:business:add')" )
    @Log(title = "赞助商主" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PmBusiness pmBusiness) {
        redisCache.deleteObject(Constants.BUSINESS_KEY);
        return toAjax(iPmBusinessService.save(pmBusiness) ? 1 : 0);
    }

    /**
     * 修改赞助商主
     */
    // @PreAuthorize("@ss.hasPermi('master:business:edit')" )
    @Log(title = "赞助商主" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PmBusiness pmBusiness) {
        redisCache.deleteObject(Constants.BUSINESS_KEY);
        return toAjax(iPmBusinessService.updateById(pmBusiness) ? 1 : 0);
    }

    /**
     * 删除赞助商主
     */
    // @PreAuthorize("@ss.hasPermi('master:business:remove')" )
    @Log(title = "赞助商主" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}" )
    public AjaxResult remove(@PathVariable Long[] ids) {
        redisCache.deleteObject(Constants.BUSINESS_KEY);
        return toAjax(iPmBusinessService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
    }
}
