package com.ruoyi.web.controller.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.api.dto.ActivityDto;
import com.ruoyi.api.dto.ClubDto;
import com.ruoyi.api.service.IMiniClubService;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.ActivityEnum;
import com.ruoyi.master.domain.PmActivity;
import com.ruoyi.master.service.IPmActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/club")
public class MiniClubController {

	@Autowired
	private IMiniClubService miniClubService;

	@Autowired
	private IPmActivityService activityService;

	/**
	 * 获取关注社团信息
	 */
	@GetMapping(value = "/{id}")
	@ResponseBody
	public AjaxResult getInfo(@PathVariable("id") Long userId) {
		return AjaxResult.success(miniClubService.queryClubList(userId));
	}

	/**
	 * 根据社团ID获取社团详情
	 */
	@GetMapping(value = "/clubDetail" )
	@ResponseBody
	public AjaxResult getActivityDetailInfo(Long clubId, Long userId) {
		ClubDto clubDto = miniClubService.getClubDetailById(clubId);
		LambdaQueryWrapper<PmActivity> activityLambdaQueryWrapper = Wrappers.lambdaQuery();
		activityLambdaQueryWrapper.eq(PmActivity::getClubId, clubId)
				.ge(PmActivity::getEnddate, new Date())
				.eq(PmActivity::getState, ActivityEnum.ActivityStateEnum.SUCCED.getValue());
		// 近期活动查询
		List<PmActivity> recentActivity = activityService.list(activityLambdaQueryWrapper);
		// 设置图片前缀
		if (!io.jsonwebtoken.lang.Collections.isEmpty(recentActivity)) {
			recentActivity.forEach(activityDto -> {
				activityDto.setPicture(Constants.IMG_PREFIX + activityDto.getPicture());
			});
		}
		// 往期图片查询
		LambdaQueryWrapper<PmActivity> activityLambdaQueryWrapper2 = Wrappers.lambdaQuery();
		activityLambdaQueryWrapper2.eq(PmActivity::getClubId, clubId)
				.le(PmActivity::getEnddate, new Date())
				.eq(PmActivity::getState, ActivityEnum.ActivityStateEnum.SUCCED.getValue());
		List<PmActivity> expireActivity = activityService.list(activityLambdaQueryWrapper2);
		// 设置图片前缀
		if (!io.jsonwebtoken.lang.Collections.isEmpty(expireActivity)) {
			expireActivity.forEach(activityDto -> {
				activityDto.setPicture(Constants.IMG_PREFIX + activityDto.getPicture());
			});
		}
		// 判断用户是否已关注社团
		LambdaQueryWrapper<ClubDto> queryWrapper = Wrappers.lambdaQuery();
		queryWrapper.eq(ClubDto::getClubId, clubId)
				.eq(ClubDto::getUserId, userId);
		clubDto.setIsFollow(miniClubService.count(queryWrapper) > 0);
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.put("clubInfo", clubDto);
		ajaxResult.put("recentActivity", recentActivity);
		ajaxResult.put("expireActivity", expireActivity);
		return AjaxResult.success(ajaxResult);
	}

	//根据用户id和社团id，完成关联
	@GetMapping(value = "/follow")
	@ResponseBody
	public AjaxResult follow(@RequestParam("userId") Long userId,
							 @RequestParam("clubId") Long clubId,
							 @RequestParam("operation") Boolean operation) {

		Integer num = miniClubService.doFollow(userId, clubId, operation);
		if (num == 1) {
			return AjaxResult.success();
		} else {
			return AjaxResult.error("关注设置异常");
		}
	}

}
