package com.ruoyi.quartz.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.api.dto.PmSearch;
import com.ruoyi.api.dto.PmTimesActivity;
import com.ruoyi.api.service.IMiniActivityService;
import com.ruoyi.api.service.IMiniSearchService;
import com.ruoyi.api.service.IPmSearchService;
import com.ruoyi.api.service.IPmTimesActivityService;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.redis.RedisCache;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.ruoyi.api.listener.ActivityListener.updateActivityTimesMap;
import static com.ruoyi.api.listener.SearchtextListener.updateSearchTimesMap;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{

    @Autowired
    private IPmSearchService searchTextService;

    @Autowired
    private IPmTimesActivityService timesActivityService;

    @Autowired
    private IMiniActivityService miniActivityService;

    @Autowired
    private IMiniSearchService searchService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private RabbitListenerEndpointRegistry rabbitListenerEndpointRegistry;

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }

    /**
     * 热榜活动
     */
    public void getMaxTimeActivityList() {
        redisCache.deleteObject(Constants.HOT_ACTIVITY_KEY);
        redisCache.setCacheList(Constants.HOT_ACTIVITY_KEY,miniActivityService.maxTimesActivityList());
    }

    /**
     * 热搜词汇
     */
    public void getHotSearchText() {
        redisCache.deleteObject(Constants.HOT_SEARCH_KEY);
        redisCache.setCacheList(Constants.HOT_SEARCH_KEY,searchService.getHotSearchText());
    }

    /**
     * 活动访问次数更新
     */
    public void updateActivityTimes() {
        try {
            this.rabbitListenerEndpointRegistry.stop();
            List<PmTimesActivity> insertActivityTimes = new ArrayList<>();
            List<PmTimesActivity> updateActivityTimes = new ArrayList<>();
            for (Long key : updateActivityTimesMap.keySet()) {
                LambdaQueryWrapper<PmTimesActivity> queryWrapper = Wrappers.lambdaQuery();
                queryWrapper.eq(PmTimesActivity::getActivityId, key);
                PmTimesActivity pmTimesActivity = timesActivityService.getOne(queryWrapper);
                if (pmTimesActivity != null) {
                    pmTimesActivity.setTimes(pmTimesActivity.getTimes() + updateActivityTimesMap.get(key));
                    updateActivityTimes.add(pmTimesActivity);
                } else {
                    PmTimesActivity newTimesActivity = new PmTimesActivity();
                    newTimesActivity.setActivityId(key).setTimes(1);
                    insertActivityTimes.add(newTimesActivity);
                }
            }
            updateActivityTimesMap.clear();
            if (!insertActivityTimes.isEmpty()) {
                timesActivityService.saveBatch(insertActivityTimes);
            }
            if (!updateActivityTimes.isEmpty()) {
                timesActivityService.updateBatchById(updateActivityTimes);
            }
            this.rabbitListenerEndpointRegistry.start();
        } catch (Exception e) {
            System.out.println("RabbitMq异常");
        }

    }

    /**
     * 搜索词汇次数更新
     */
    public void updateSearchTextTimes() {
        try {
            this.rabbitListenerEndpointRegistry.stop();
            List<PmSearch> insertSearchTimes = new ArrayList<>();
            List<PmSearch> updateSearchTimes = new ArrayList<>();
            for (String key : updateSearchTimesMap.keySet()) {
                LambdaQueryWrapper<PmSearch> queryWrapper = Wrappers.lambdaQuery();
                queryWrapper.eq(PmSearch::getSearchText, key);
                PmSearch pmSearch = searchTextService.getOne(queryWrapper);
                if (pmSearch != null) {
                    pmSearch.setTimes(pmSearch.getTimes() + updateSearchTimesMap.get(key));
                    updateSearchTimes.add(pmSearch);
                } else {
                    PmSearch newTimesSearchText = new PmSearch();
                    newTimesSearchText.setSearchText(key).setTimes(1);
                    insertSearchTimes.add(newTimesSearchText);
                }
            }
            updateSearchTimesMap.clear();
            if (!insertSearchTimes.isEmpty()) {
                searchTextService.saveBatch(insertSearchTimes);
            }
            if (!updateSearchTimes.isEmpty()) {
                searchTextService.updateBatchById(updateSearchTimes);
            }
            this.rabbitListenerEndpointRegistry.start();
        } catch (Exception e) {
            System.out.println("RabbitMq异常");
        }

    }
}
