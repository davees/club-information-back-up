import request from '@/utils/request'

// 查询活动主列表
export function listActivity(query) {
  return request({
    url: '/master/activity/list',
    method: 'get',
    params: query
  })
}


// 导出活动主
export function exportActivity(query) {
  return request({
    url: '/system/activity/export',
    method: 'get',
    params: query
  })
}

// 活动提交审批
export function approvalActivity(id) {
  return request({
    url: '/master/activity/' + id,
    method: 'get'
  })
}

// 批量活动提交审批
export function approvalBatchActivity(ids) {
  return request({
    url: '/master/activity/batchApproval/' + ids,
    method: 'get'
  })
}


// 查询活动审批记录详情
export function approvalActivityLog(id) {
  return request({
    url: '/master/activity/logList/' + id,
    method: 'get'
  })
}

