import request from '@/utils/request'

// 查询活动主列表
export function listMyActivitys(query) {
  return request({
    url: '/master/myActivity/list',
    method: 'get',
    params: query
  })
}

// 查询活动主详细
export function getMyActivity(id) {
  return request({
    url: '/master/myActivity/' + id,
    method: 'get'
  })
}

// 新增活动主
export function addMyActivity(data) {
  return request({
    url: '/master/myActivity',
    method: 'post',
    data: data
  })
}

// 修改活动主
export function updateMyActivity(data) {
  return request({
    url: '/master/myActivity',
    method: 'put',
    data: data
  })
}

// 删除活动主
export function delMyActivity(id) {
  return request({
    url: '/master/myActivity/' + id,
    method: 'delete'
  })
}

// 删除活动主
export function approvalMyActivity(id) {
  return request({
    url: '/master/myActivity/approval/' + id,
    method: 'get'
  })
}