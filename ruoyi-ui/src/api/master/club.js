import request from '@/utils/request'

// 查询社团列表
export function listClub(query) {
  return request({
    url: '/master/club/list',
    method: 'get',
    params: query
  })
}

// 查询社团详细
export function getClub(id) {
  return request({
    url: '/master/club/' + id,
    method: 'get'
  })
}

// 新增社团
export function addClub(data) {
  return request({
    url: '/master/club',
    method: 'post',
    data: data
  })
}

// 查询社团详细
export function getClubAdminList() {
  return request({
    url: '/system/user/userList',
    method: 'get'
  })
}

// 修改社团
export function updateClub(data) {
  return request({
    url: '/master/club',
    method: 'put',
    data: data
  })
}

// 删除社团
export function delClub(id) {
  return request({
    url: '/master/club/' + id,
    method: 'delete'
  })
}

// 导出社团
export function exportClub(query) {
  return request({
    url: '/master/club/export',
    method: 'get',
    params: query
  })
}
