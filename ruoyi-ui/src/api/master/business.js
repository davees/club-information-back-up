import request from '@/utils/request'

// 查询赞助商主列表
export function listBusiness(query) {
  return request({
    url: '/master/business/list',
    method: 'get',
    params: query
  })
}

// 查询赞助商主详细
export function getBusiness(id) {
  return request({
    url: '/master/business/' + id,
    method: 'get'
  })
}

// 新增赞助商主
export function addBusiness(data) {
  return request({
    url: '/master/business',
    method: 'post',
    data: data
  })
}

// 修改赞助商主
export function updateBusiness(data) {
  return request({
    url: '/master/business',
    method: 'put',
    data: data
  })
}

// 删除赞助商主
export function delBusiness(id) {
  return request({
    url: '/master/business/' + id,
    method: 'delete'
  })
}

// 导出赞助商主
export function exportBusiness(query) {
  return request({
    url: '/master/business/export',
    method: 'get',
    params: query
  })
}





