package com.ruoyi.approval.result;

import lombok.Data;

/**
 * @Date 2021/3/27 20:18
 * @Created mobian
 * @Description 提交审批返回的结果信息
 */
@Data
public class ApprovalResult {

	//审批成功
	public static final String APPROVAL_SUSSESS = "20";

	//审批失败
	public static final String APPROVAL_FAIL = "10";


	private String code;
	private String message;



	public ApprovalResult() {
	}

	public ApprovalResult(String code, String message) {
		this.code = code;
		this.message = message;
	}
}
