package com.ruoyi.approval.result;

/**
 * @Date 2021/3/31 16:53
 * @Created mobian
 * @Description 审批结果的返回信息
 */
public class ApprovalResultFinalMessage {

	public static final String APPROVAL_SUCCESS = "审批成功";
	public static final String APPROVAL_FAILE = "审批失败";

	public static final String NEED_TEACHER_FAILD = "缺少带老师";

	public static final String NOT_ALLOW_OUT_FAILD = "人数过多，不允许出校门";


	public static final String CONTENT_NOT_ALLOW_FAILD = "活动详情不允许为空";




}
