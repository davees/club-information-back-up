package com.ruoyi.approval.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Date 2021/3/31 16:59
 * @Created mobian
 * @Description 审批日志详情表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApprovalResultLog {

	private Integer id;

	private String status;

	private String log;

	private String createTime;

	private String approvalId;

	private String approvalName;
}
