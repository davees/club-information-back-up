package com.ruoyi.approval.pojo;

import com.ruoyi.common.enums.ActivityEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Date 2021/3/28 11:08
 * @Created mobian
 * @Description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityApprovalPojo extends BaseApproval {

	//基础属性

	//内容详情要素
	private String approvalDetail;

	//活动开始时间
	private Date approvalBeginDate;

	//活动结束时间
	private Date approvalEndDate;

	//活动规模,人数
	private String approvalScale;

	//活动场地,校外、校内
	private String approvalPlace;

	//活动类型
	private Integer approvalType;

	//带队老师
	private String leadTeacher;
}
