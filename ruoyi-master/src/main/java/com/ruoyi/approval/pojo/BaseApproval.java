package com.ruoyi.approval.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Date 2021/3/28 11:03
 * @Created mobian
 * @Description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseApproval {

	//主键id
	private String id;

	//审批状态
	private Integer status;

	//审批类型
	private String type;

	//审批时间
	private Date approvalCreateDate;

	//更新时间
	private Date approvalUpdateDate;

	//审批对象的id
	private String approvalId;
}
