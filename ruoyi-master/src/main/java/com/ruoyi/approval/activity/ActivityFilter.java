package com.ruoyi.approval.activity;

import com.ruoyi.approval.pojo.ActivityApprovalPojo;
import com.ruoyi.approval.result.ApprovalResult;

import java.text.ParseException;
import java.util.List;

/**
 * @Date 2021/3/25 20:51
 * @Created mobian
 * @Description 活动和商家审批接口
 */
public interface ActivityFilter {

	List<ApprovalResult> doFilter(ActivityApprovalPojo approvalPojo) throws ParseException;

}
