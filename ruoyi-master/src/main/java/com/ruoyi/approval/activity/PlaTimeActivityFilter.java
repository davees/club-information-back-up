package com.ruoyi.approval.activity;

import com.ruoyi.approval.mapper.ApprovalMapper;
import com.ruoyi.approval.pojo.ActivityApprovalPojo;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.approval.result.ApprovalResultFinalMessage;
import com.ruoyi.approval.result.ApprovalResultLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Date 2021/3/25 20:54
 * @Created mobian
 * @Description 活动地点审查:时间和场地
 */
@Component
public class PlaTimeActivityFilter implements ActivityFilter {
	private static final Logger logger = LoggerFactory.getLogger(PlaTimeActivityFilter.class);


	@Autowired
	private ApprovalMapper approvalMapper;

	@Override
	public List<ApprovalResult> doFilter(ActivityApprovalPojo pmActivity) throws ParseException {
		List<ApprovalResult> results = new ArrayList<>();
		ApprovalResult result = new ApprovalResult();
		String approvalId = pmActivity.getApprovalId();
		logger.info("执行了活动地点的审查");


		Date beginDate = pmActivity.getApprovalBeginDate();
		Date endDate = pmActivity.getApprovalEndDate();

		//1 校外 0校内
		String place = pmActivity.getApprovalPlace();
		//审批对象的id


		if (beginDate == null && endDate == null) {
			result.setCode(ApprovalResult.APPROVAL_FAIL);
			result.setMessage(ApprovalResultFinalMessage.APPROVAL_FAILE);

		} else {
			//默认审批规则
			result.setCode(ApprovalResult.APPROVAL_SUSSESS);
			result.setMessage(ApprovalResultFinalMessage.APPROVAL_SUCCESS);
		}
		saveApprovalLog(result, approvalId);
		results.add(result);

		return results;
	}


	//审批信息持久化
	public void saveApprovalLog(ApprovalResult result, String approvalId) throws ParseException {
		ApprovalResultLog log = new ApprovalResultLog();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		log.setStatus(result.getCode());
		log.setLog(result.getMessage());
		log.setCreateTime(df.format(new Date()));
		log.setApprovalId(approvalId);
		log.setApprovalName("ContentActivity");
		approvalMapper.insertApprovalLog(log);
	}
}
