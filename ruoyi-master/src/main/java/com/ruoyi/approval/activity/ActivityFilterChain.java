package com.ruoyi.approval.activity;

import com.ruoyi.approval.pojo.ActivityApprovalPojo;
import com.ruoyi.approval.result.ApprovalResult;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Date 2021/3/28 11:59
 * @Created mobian
 * @Description 审批链
 */
@Component
public class ActivityFilterChain implements ActivityFilter {
	List<ActivityFilter> filterList = new ArrayList<>();

	//将具体的操作类添加到该操作的链条中
	public ActivityFilterChain addFilter(ActivityFilter filter) {
		filterList.add(filter);
		return this;
	}


	@Override
	public List<ApprovalResult> doFilter(ActivityApprovalPojo approvalPojo) throws ParseException {

		ArrayList<ApprovalResult> approvalResultsList = new ArrayList<>();

		for (ActivityFilter filter : filterList) {
			List<ApprovalResult> results = filter.doFilter(approvalPojo);
			if (results != null) {
				approvalResultsList.add(results.get(0));
			}
		}
		return approvalResultsList;
	}
}
