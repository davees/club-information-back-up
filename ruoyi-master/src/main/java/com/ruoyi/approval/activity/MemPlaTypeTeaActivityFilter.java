package com.ruoyi.approval.activity;

import com.ruoyi.common.enums.ActivityEnum;
import com.ruoyi.approval.mapper.ApprovalMapper;
import com.ruoyi.approval.pojo.ActivityApprovalPojo;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.approval.result.ApprovalResultFinalMessage;
import com.ruoyi.approval.result.ApprovalResultLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Date 2021/3/25 20:54
 * @Created mobian
 * @Description 活动人数审查：人、场地、类型、老师
 */
@Component
public class MemPlaTypeTeaActivityFilter implements ActivityFilter {

	private static final Logger logger = LoggerFactory.getLogger(MemPlaTypeTeaActivityFilter.class);

	//审批规则：人、场地、类型、老师
	//1000人，校外，教育
	//

	@Autowired
	private ApprovalMapper approvalMapper;

	@Override
	public List<ApprovalResult> doFilter(ActivityApprovalPojo pmActivity) throws ParseException {
		List<ApprovalResult> results = new ArrayList<>();
		ApprovalResult result = new ApprovalResult();
		logger.info("执行了活动人数的审查");

		Integer people = Integer.parseInt(pmActivity.getApprovalScale());
		String leadTeacher = pmActivity.getLeadTeacher();
		// ActivityEnum.ActivityTypeEnum type = pmActivity.getApprovalType();
		//0 校外 1校内
		String place = pmActivity.getApprovalPlace();

		//审批对象的id
		String approvalId = pmActivity.getApprovalId();

		if ("0".equals(place)) {
			if (leadTeacher == null) {
				result.setCode(ApprovalResult.APPROVAL_FAIL);
				result.setMessage(ApprovalResultFinalMessage.NEED_TEACHER_FAILD);
			}
		} else if (people > 1000) {
			if ("0".equals(place)) {
				result.setCode(ApprovalResult.APPROVAL_FAIL);
				result.setMessage(ApprovalResultFinalMessage.NOT_ALLOW_OUT_FAILD);
			}
		} else {
			//默认审批规则
			result.setCode(ApprovalResult.APPROVAL_SUSSESS);
			result.setMessage(ApprovalResultFinalMessage.APPROVAL_SUCCESS);
		}

		saveApprovalLog(result, approvalId);
		results.add(result);

		return results;
	}


	//审批信息持久化
	public void saveApprovalLog(ApprovalResult result, String approvalId) throws ParseException {
		ApprovalResultLog log = new ApprovalResultLog();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		log.setStatus(result.getCode());
		log.setLog(result.getMessage());
		log.setCreateTime(df.format(new Date()));
		log.setApprovalId(approvalId);
		log.setApprovalName("MemPlaTypeTeaActivity");

		approvalMapper.insertApprovalLog(log);
	}
}
