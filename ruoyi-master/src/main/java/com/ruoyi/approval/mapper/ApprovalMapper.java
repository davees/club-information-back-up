package com.ruoyi.approval.mapper;

import com.ruoyi.approval.pojo.ActivityApprovalPojo;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.approval.result.ApprovalResultLog;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Date 2021/3/28 14:10
 * @Created mobian
 * @Description
 */
@Component
public interface ApprovalMapper {

	List<ActivityApprovalPojo> queryApprovalRecordList(String approvalId);

	List<ApprovalResultLog> queryApprovalLogList(String approvalId);

	void insertApprovalLog(ApprovalResultLog log);

	void insertApprovalRecord(ActivityApprovalPojo pojo);

	//删除审批记录
	void deleteApprovalRecord(String approvalId);
	//删除审批日志记录
	void deleteApprovalLog(String approvalId);

}
