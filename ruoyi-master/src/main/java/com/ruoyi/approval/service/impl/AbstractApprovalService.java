package com.ruoyi.approval.service.impl;

import com.ruoyi.approval.pojo.ActivityApprovalPojo;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.master.domain.PmActivity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;

/**
 * @Date 2021/3/28 11:15
 * @Created mobian
 * @Description 模板方法,审批操作
 */
public
class AbstractApprovalService {

	private static final Logger logger = LoggerFactory.getLogger(AbstractApprovalService.class);

	//构建审批对象

	//执行对应的执行审批操作

	//定义的模板方法
	@Transactional
	public List<ApprovalResult> doApproval(PmActivity pmActivity) throws ParseException {
		logger.info("开始构建审批对象...");
		ActivityApprovalPojo newRecord = buildApprovalPojo(pmActivity);
		logger.info("开始执行审批...");
		return executeApproval(newRecord);
	}

	//子类实现,构建审批对象
	protected ActivityApprovalPojo buildApprovalPojo(PmActivity pmActivity) throws ParseException {
		throw new RuntimeException("子类必须实现该方法");
	}

	//子类实现,构建审批链
	protected List<ApprovalResult> executeApproval(ActivityApprovalPojo approvalPojo) throws ParseException {
		throw new RuntimeException("子类必须实现该方法");
	}
}
