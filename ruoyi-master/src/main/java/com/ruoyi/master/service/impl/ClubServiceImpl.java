package com.ruoyi.master.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.master.domain.Club;
import com.ruoyi.master.mapper.ClubMapper;
import com.ruoyi.master.service.IClubService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 社团Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class ClubServiceImpl extends ServiceImpl<ClubMapper, Club> implements IClubService {

    @Override
    public List<Club> queryList(Club club) {
        LambdaQueryWrapper<Club> lqw = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(club.getName())){
            lqw.like(Club::getName ,club.getName());
        }
        if (StringUtils.isNotBlank(club.getInstruction())){
            lqw.eq(Club::getInstruction ,club.getInstruction());
        }
        return this.list(lqw);
    }
}
