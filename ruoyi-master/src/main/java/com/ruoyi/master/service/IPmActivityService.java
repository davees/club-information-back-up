package com.ruoyi.master.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.master.domain.PmActivity;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

/**
 * 活动主Service接口
 *
 * @author ruoyi
 * @date 2021-03-25
 */
public interface IPmActivityService extends IService<PmActivity> {

    /**
     * 查询列表
     */
    List<HashMap<String, Object>> queryList(HashMap<String, Object> map);


    List<ApprovalResult> approvalActivity(Integer id) throws ParseException;

    //根据查询对应的审批状态
    Integer queryApprovalStatus(Long id);
}
