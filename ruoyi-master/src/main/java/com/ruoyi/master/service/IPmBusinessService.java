package com.ruoyi.master.service;

import com.ruoyi.master.domain.PmBusiness;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 赞助商主Service接口
 *
 * @author ruoyi
 * @date 2021-03-26
 */
public interface IPmBusinessService extends IService<PmBusiness> {

    /**
     * 查询列表
     */
    List<PmBusiness> queryList(PmBusiness pmBusiness);
}
