package com.ruoyi.master.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.master.domain.Club;

import java.util.List;

/**
 * 社团Service接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface IClubService extends IService<Club> {

    /**
     * 查询列表
     */
    List<Club> queryList(Club club);
}
