package com.ruoyi.master.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.ExceptionFinalMessage;
import com.ruoyi.approval.result.ApprovalResult;
import com.ruoyi.approval.service.impl.ActivityApprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.master.mapper.PmActivityMapper;
import com.ruoyi.master.domain.PmActivity;
import com.ruoyi.master.service.IPmActivityService;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

/**
 * 活动主Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Service
public class PmActivityServiceImpl extends ServiceImpl<PmActivityMapper, PmActivity> implements IPmActivityService {


	@Autowired
	private PmActivityMapper pmActivityMapper;

	@Autowired
	private ActivityApprovalService approvalService;


	@Override
	public List<HashMap<String, Object>> queryList(HashMap<String, Object> map) {

		return pmActivityMapper.queryList(map);
	}


	//活动提交审批

	public List<ApprovalResult> approvalActivity(Integer id) throws ParseException {

		List<PmActivity> pmActivities = pmActivityMapper.queryActivityList(id);
		if (pmActivities == null || pmActivities.size() != 1) {
			throw new RuntimeException(ExceptionFinalMessage.DATA_IS_ERROR + ":pmActivities");
		}
		//审批对象__提交审批
		PmActivity pmActivity = pmActivities.get(0);
		return approvalService.doApproval(pmActivity);
	}

	@Override
	public Integer queryApprovalStatus(Long id) {
		return pmActivityMapper.queryApprovalStatus(id);
	}
}
