package com.ruoyi.master.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import com.ruoyi.master.mapper.PmBusinessMapper;
import com.ruoyi.master.domain.PmBusiness;
import com.ruoyi.master.service.IPmBusinessService;

import java.util.List;
import java.util.Map;

/**
 * 赞助商主Service业务层处理
 *
 * @author ruoyi
 * @date 2021-03-26
 */
@Service
public class PmBusinessServiceImpl extends ServiceImpl<PmBusinessMapper, PmBusiness> implements IPmBusinessService {

    @Override
    public List<PmBusiness> queryList(PmBusiness pmBusiness) {
        LambdaQueryWrapper<PmBusiness> lqw = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(pmBusiness.getPlace())){
            lqw.eq(PmBusiness::getPlace ,pmBusiness.getPlace());
        }
        if (StringUtils.isNotBlank(pmBusiness.getName())){
            lqw.like(PmBusiness::getName ,pmBusiness.getName());
        }
        if (StringUtils.isNotBlank(pmBusiness.getTel1())){
            lqw.eq(PmBusiness::getTel1 ,pmBusiness.getTel1());
        }
        if (StringUtils.isNotBlank(pmBusiness.getTel2())){
            lqw.eq(PmBusiness::getTel2 ,pmBusiness.getTel2());
        }
        if (StringUtils.isNotBlank(pmBusiness.getPicture())){
            lqw.eq(PmBusiness::getPicture ,pmBusiness.getPicture());
        }
        if (StringUtils.isNotBlank(pmBusiness.getScale())){
            lqw.eq(PmBusiness::getScale ,pmBusiness.getScale());
        }
        if (StringUtils.isNotBlank(pmBusiness.getType())){
            lqw.eq(PmBusiness::getType ,pmBusiness.getType());
        }
        if (StringUtils.isNotBlank(pmBusiness.getBusinesscontext())){
            lqw.eq(PmBusiness::getBusinesscontext ,pmBusiness.getBusinesscontext());
        }
        if (pmBusiness.getBegindate() != null){
            lqw.eq(PmBusiness::getBegindate ,pmBusiness.getBegindate());
        }
        if (pmBusiness.getEnddate() != null){
            lqw.eq(PmBusiness::getEnddate ,pmBusiness.getEnddate());
        }
        if (pmBusiness.getCreaterdate() != null){
            lqw.eq(PmBusiness::getCreaterdate ,pmBusiness.getCreaterdate());
        }
        if (pmBusiness.getUpdatetime() != null){
            lqw.eq(PmBusiness::getUpdatetime ,pmBusiness.getUpdatetime());
        }
        if (StringUtils.isNotBlank(pmBusiness.getContacts())){
            lqw.eq(PmBusiness::getContacts ,pmBusiness.getContacts());
        }
        return this.list(lqw);
    }
}
