package com.ruoyi.master.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * 赞助商主对象 pm_business
 *
 * @author ruoyi
 * @date 2021-03-26
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pm_business")
public class PmBusiness implements Serializable {

private static final long serialVersionUID=1L;


    /** 赞助商主键id */
    @TableId(value = "ID")
    private Long id;

    /** 商家地址 */
    @Excel(name = "商家地址")
    private String place;

    /** 商家名称 */
    @Excel(name = "商家名称")
    private String name;

    /** 联系方式1 */
    @Excel(name = "联系方式1")
    private String tel1;

    /** 联系方式2 */
    @Excel(name = "联系方式2")
    private String tel2;

    /** 商家图片 */
    @Excel(name = "商家图片")
    private String picture;

    /** 商家规模 */
    @Excel(name = "商家规模")
    private String scale;

    /** 商家类型 */
    @Excel(name = "商家类型")
    private String type;

    /** 宣传详情 */
    @Excel(name = "宣传详情")
    private String businesscontext;

    /** 活动开始时间 */
    @Excel(name = "活动开始时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date begindate;

    /** 活动结束时间 */
    @Excel(name = "活动结束时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date enddate;

    /** 赞助商添加时间 */
    @Excel(name = "赞助商添加时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createrdate;

    /** 赞助商修改要素时间 */
    @Excel(name = "赞助商修改要素时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatetime;

    /** 商家联系人名字 */
    @Excel(name = "商家联系人名字")
    private String contacts;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
