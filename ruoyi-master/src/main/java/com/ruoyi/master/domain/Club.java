package com.ruoyi.master.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.core.domain.entity.SysUser;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;

/**
 * 社团对象 club
 *
 * @author ruoyi
 * @date 2021-03-24
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("mp_club")
public class Club implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID")
    private Long id;

    private String name;

    private String instruction;

    // 社团管理员id
    private Long userId;

    private String avatar;

    private String slogan;

    @TableField(exist = false)
    private SysUser sysUser;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
