package com.ruoyi.master.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

/**
 * 活动主对象 pm_activity
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pm_activity")
public class PmActivity implements Serializable {

private static final long serialVersionUID=1L;

    /** 主键 */
    @TableId(value = "ID")
    private Long id;

    /** 活动宣传图片 */
    @Excel(name = "活动宣传图片")
    private String picture;

    /** 活动标题 */
    @Excel(name = "活动标题")
    private String title;

    /** 开始时间 */
    @Excel(name = "开始时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date begindate;

    /** 结束时间 */
    @Excel(name = "结束时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date enddate;

    /** 详细活动地址 */
    private String detailPlace;

    /** 活动地点 */
    @Excel(name = "活动地点")
    private String place;

    /** 活动联系方式 */
    @Excel(name = "活动联系方式")
    private String tel;

    /** 活动规模 */
    @Excel(name = "活动规模")
    private String scale;

    /** 活动规模 */
    @Excel(name = "是否含有商业活动")
    private String isBusinessActivity;

    /** 活动规模 */
    @Excel(name = "活动类型")
    private Integer type;

    /** 活动规模 */
    @Excel(name = "是否需要商业活动赞助")
    private String isNeedBusinessActivity;

    /** 活动规模 */
    @Excel(name = "指导老师")
    private String leadTeacher;

    /** 活动规模 */
    @Excel(name = "活动详情")
    private String activityContent;


    /** 活动口号 */
    @Excel(name = "活动口号")
    private String slogan;

    /**
     * 活动状态：未审批 审批失败 审批成功
     */
    private Integer state;

    /**
     * 所属社团ID
     */
    private Long clubId;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
