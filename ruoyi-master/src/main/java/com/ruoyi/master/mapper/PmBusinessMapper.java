package com.ruoyi.master.mapper;

import com.ruoyi.master.domain.PmBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 赞助商主Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-26
 */
public interface PmBusinessMapper extends BaseMapper<PmBusiness> {

}
