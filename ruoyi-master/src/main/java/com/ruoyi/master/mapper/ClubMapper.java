package com.ruoyi.master.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.master.domain.Club;

/**
 * 社团Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ClubMapper extends BaseMapper<Club> {

}
