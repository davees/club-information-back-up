package com.ruoyi.master.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.master.domain.PmActivity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 活动主Mapper接口
 *
 * @author ruoyi
 * @date 2021-03-25
 */
@Component
public interface PmActivityMapper extends BaseMapper<PmActivity> {

	List<HashMap<String, Object>> queryList(HashMap<String, Object> map);

	List<PmActivity> queryActivityList(@Param("id") Integer id);

	void updateActivityStatusByApprovalResult(@Param("status") Integer status, @Param("approvalUpdateDate")Date approvalUpdateDate, @Param("id") Long approvalId);

	Integer queryApprovalStatus(Long id);
}
