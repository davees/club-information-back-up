package com.ruoyi.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;

/**
 * 【请填写功能名称】对象 pm_search
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pm_search")
public class PmSearch implements Serializable {

private static final long serialVersionUID=1L;


    /** id */
    private Long id;

    /** 搜索内容 */
    @Excel(name = "搜索内容")
    private String searchText;

    /** 搜索次数 */
    @Excel(name = "搜索次数")
    private Integer times;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
