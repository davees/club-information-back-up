package com.ruoyi.api.dto;

import lombok.Data;

@Data
public class SearchClubResultDto {

    private Long clubId;

    private String clubName;

}
