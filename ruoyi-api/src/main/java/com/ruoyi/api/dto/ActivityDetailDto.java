package com.ruoyi.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ActivityDetailDto {

    private Long clubId;

    private String clubAvatar;

    private String clubName;

    private String clubInstruction;

    private String activityPicture;

    private String activityTitle;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date activityBeginDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date activityEndDate;

    private String activityDetailPlace;

    private String activityContent;

    private String slogan;

    private Boolean isFollow;

}
