package com.ruoyi.api.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("pm_userclub_relation")
public class ClubDto implements Serializable {

    /** 主键id */
    @TableId(value = "ID")
    private Long id;

    /** 社团id */
    private Long clubId;

    /** 社团名称 */
    @TableField(exist = false)
    private String clubName;

    /** 社团图片 */
    @TableField(exist = false)
    private String avatar;

    /** 用户id */
    private Long userId;

    @TableField(exist = false)
    private String instruction;

    @TableField(exist = false)
    private String slogan;

    @TableField(exist = false)
    private String phoneNumber;

    private Boolean isFollow;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();

    public ClubDto(Long userId, Long clubId) {
        this.clubId = clubId;
        this.userId = userId;
    }
}
