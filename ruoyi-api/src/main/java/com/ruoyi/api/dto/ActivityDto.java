package com.ruoyi.api.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("pm_useractivity_relation")
public class ActivityDto implements Serializable {

    /** 主键id */
    @TableId(value = "ID")
    private Long id;

    /** 社团id */
    private Long activityId;

    private Long userId;

    @TableField(exist = false)
    private String picture;

    @TableField(exist = false)
    private String title;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @TableField(exist = false)
    private String detailPlace;

    @TableField(exist = false)
    private String slogan;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date releaseTime;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();

    public ActivityDto(Long userId, Long activityId) {
        this.activityId = activityId;
        this.userId = userId;
    }
}
