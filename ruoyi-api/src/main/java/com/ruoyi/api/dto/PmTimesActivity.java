package com.ruoyi.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;

/**
 * 【请填写功能名称】对象 pm_times_activity
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("pm_times_activity")
public class PmTimesActivity implements Serializable {

private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "ID")
    private Long id;

    /** 活动id */
    @Excel(name = "活动id")
    private Long activityId;

    /** 访问次数 */
    @Excel(name = "访问次数")
    private Integer times;

    @TableField(exist = false)
    private Map<String, Object> params = new HashMap<>();
}
