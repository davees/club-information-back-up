package com.ruoyi.api.dto;

import lombok.Data;

@Data
public class SearchActivityResultDto {

    private Long ActivityId;

    private String ActivityName;

}
