package com.ruoyi.api.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SearchtextListener {

    public static Map<String, Integer> updateSearchTimesMap = new HashMap<>();

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "queue.searchText", durable = "true"),
            exchange = @Exchange(
                    value = "exchange.searchText",
                    ignoreDeclarationExceptions = "true",
                    type = ExchangeTypes.TOPIC
            ),
            key = {"#.#"}))
    public void listen(String searchText){
        if (updateSearchTimesMap.containsKey(searchText)) {
            updateSearchTimesMap.put(searchText, updateSearchTimesMap.get(searchText) + 1);
        } else {
            updateSearchTimesMap.put(searchText, 1);
        }
    }

}
