package com.ruoyi.api.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ActivityListener {

    public static Map<Long, Integer> updateActivityTimesMap = new HashMap<>();

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "queue.activity", durable = "true"),
            exchange = @Exchange(
                    value = "exchange.activity",
                    ignoreDeclarationExceptions = "true",
                    type = ExchangeTypes.TOPIC
            ),
            key = {"#.#"}))
    public void listen(Long activityId){
        if (updateActivityTimesMap.containsKey(activityId)) {
            updateActivityTimesMap.put(activityId, updateActivityTimesMap.get(activityId) + 1);
        } else {
            updateActivityTimesMap.put(activityId, 1);
        }
    }

}
