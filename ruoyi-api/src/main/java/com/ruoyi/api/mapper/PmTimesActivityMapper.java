package com.ruoyi.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.api.dto.PmTimesActivity;

/**
 * Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-30
 */
public interface PmTimesActivityMapper extends BaseMapper<PmTimesActivity> {

}
