package com.ruoyi.api.mapper;

import com.ruoyi.api.dto.SearchActivityResultDto;
import com.ruoyi.api.dto.SearchClubResultDto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MiniSearchMapper {

    List<SearchActivityResultDto> queryActivityByName(String searchText);

    List<SearchClubResultDto> queryClubByName(String searchText);

    List<String> queryHotSearchText();
}
