package com.ruoyi.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.api.dto.ActivityDetailDto;
import com.ruoyi.api.dto.ActivityDto;

import java.util.List;

public interface MiniActivityMapper extends BaseMapper<ActivityDto> {

    List<ActivityDto> queryActivityList(Long userId);

    //热榜活动数据
    List<ActivityDto> queryMaxTimesActivityList();

    ActivityDetailDto queryActivityDetailList(Long activityId);

    List<ActivityDto> queryClubActivitys(Long userId);
}
