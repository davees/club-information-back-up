package com.ruoyi.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.api.dto.PmSearch;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-30
 */
public interface PmSearchMapper extends BaseMapper<PmSearch> {

}
