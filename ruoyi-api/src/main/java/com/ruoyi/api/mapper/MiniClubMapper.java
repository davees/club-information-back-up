package com.ruoyi.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.api.dto.ClubDto;
import com.ruoyi.master.domain.Club;

import java.util.List;

public interface MiniClubMapper extends BaseMapper<ClubDto> {


    List<ClubDto> queryClubList(Long userId);

    ClubDto queryClubById(Long clubId);
}
