package com.ruoyi.api.service.impl;

import com.ruoyi.api.dto.PmTimesActivity;
import com.ruoyi.api.mapper.PmTimesActivityMapper;
import com.ruoyi.api.service.IPmTimesActivityService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
import java.util.Map;

/**
 * 活动次数Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-30
 */
@Service
public class PmTimesActivityServiceImpl extends ServiceImpl<PmTimesActivityMapper, PmTimesActivity> implements IPmTimesActivityService {

}
