package com.ruoyi.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.api.dto.ClubDto;
import com.ruoyi.api.mapper.MiniClubMapper;
import com.ruoyi.api.service.IMiniClubService;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.master.domain.Club;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class MiniClubServiceImpl extends ServiceImpl<MiniClubMapper, ClubDto> implements IMiniClubService {

    @Override
    public List<ClubDto> queryClubList(Long userId) {
    	// 根据用户ID查询关注社团信息
        List<ClubDto> clubDtos = baseMapper.queryClubList(userId);
		if (!io.jsonwebtoken.lang.Collections.isEmpty(clubDtos)) {
			// 社团图片地址
			clubDtos.forEach(clubDto -> clubDto.setAvatar(Constants.IMG_PREFIX + clubDto.getAvatar()));
		}
        return clubDtos;
    }

	@Override
	public Integer doFollow(Long userId, Long clubId, Boolean operation) {
		int num = 0;
		if (operation != null && !operation) {
			num = baseMapper.insert(new ClubDto(userId, clubId));
		}
		if (operation != null && operation) {
			HashMap<String, Object> condMap = new HashMap<>();
			condMap.put("USER_ID", userId);
			condMap.put("CLUB_ID", clubId);
			num = baseMapper.deleteByMap(condMap);
		}
		return num;
	}

	@Override
	public ClubDto getClubDetailById(Long clubId) {
		ClubDto clubDto = baseMapper.queryClubById(clubId);
		clubDto.setAvatar(Constants.IMG_PREFIX+clubDto.getAvatar());
		return clubDto;
	}
}
