package com.ruoyi.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.api.dto.PmSearch;

import java.util.List;

/**
 * Service接口
 *
 * @author ruoyi
 * @date 2021-04-30
 */
public interface IPmSearchService extends IService<PmSearch> {

    /**
     * 查询列表
     */
    List<PmSearch> queryList(PmSearch pmSearch);
}
