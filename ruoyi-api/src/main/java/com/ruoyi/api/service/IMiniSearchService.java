package com.ruoyi.api.service;

import com.ruoyi.api.dto.SearchActivityResultDto;
import com.ruoyi.api.dto.SearchClubResultDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IMiniSearchService {

    List<SearchActivityResultDto> searchActivityByName(String searchText);

    List<SearchClubResultDto> searchClubByName(String searchText);

    List<String> getHotSearchText();

}
