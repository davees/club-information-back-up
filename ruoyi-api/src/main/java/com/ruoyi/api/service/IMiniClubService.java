package com.ruoyi.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.api.dto.ClubDto;
import com.ruoyi.master.domain.Club;

import java.util.List;

public interface IMiniClubService extends IService<ClubDto> {


    List<ClubDto> queryClubList(Long userId);

    Integer doFollow(Long userId, Long clubId, Boolean operation);

    ClubDto getClubDetailById(Long clubId);
}
