package com.ruoyi.api.service.impl;

import com.ruoyi.api.dto.SearchActivityResultDto;
import com.ruoyi.api.dto.SearchClubResultDto;
import com.ruoyi.api.mapper.MiniSearchMapper;
import com.ruoyi.api.service.IMiniSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MiniSearchServiceImpl implements IMiniSearchService {

    @Autowired
    private MiniSearchMapper searchMapper;

    @Override
    public List<SearchActivityResultDto> searchActivityByName(String searchText) {
        return searchMapper.queryActivityByName(searchText);
    }

    @Override
    public List<SearchClubResultDto> searchClubByName(String searchText) {
        return searchMapper.queryClubByName(searchText);
    }

    @Override
    public List<String> getHotSearchText() {
        return searchMapper.queryHotSearchText();
    }

}
