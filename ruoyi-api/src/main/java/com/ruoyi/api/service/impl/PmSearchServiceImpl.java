package com.ruoyi.api.service.impl;

import com.ruoyi.api.dto.PmSearch;
import com.ruoyi.api.mapper.PmSearchMapper;
import com.ruoyi.api.service.IPmSearchService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-30
 */
@Service
public class PmSearchServiceImpl extends ServiceImpl<PmSearchMapper, PmSearch> implements IPmSearchService {

    @Override
    public List<PmSearch> queryList(PmSearch pmSearch) {
        LambdaQueryWrapper<PmSearch> lqw = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(pmSearch.getSearchText())){
            lqw.eq(PmSearch::getSearchText ,pmSearch.getSearchText());
        }
        if (pmSearch.getTimes() != null){
            lqw.eq(PmSearch::getTimes ,pmSearch.getTimes());
        }
        return this.list(lqw);
    }
}
