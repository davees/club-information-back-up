package com.ruoyi.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.api.dto.PmTimesActivity;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2021-04-30
 */
public interface IPmTimesActivityService extends IService<PmTimesActivity> {

}
