package com.ruoyi.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.api.dto.ActivityDetailDto;
import com.ruoyi.api.dto.ActivityDto;

import java.util.List;

public interface IMiniActivityService extends IService<ActivityDto> {

    List<ActivityDto> queryActivityList(Long userId);

    ActivityDetailDto queryActivityDetailList(Long activityId, Long userId);

	Integer doFollow(Long userId, Long clubId, Boolean operation);

	List<ActivityDto> maxTimesActivityList();

    List<ActivityDto> getClubActivitys(Long userId);
}
