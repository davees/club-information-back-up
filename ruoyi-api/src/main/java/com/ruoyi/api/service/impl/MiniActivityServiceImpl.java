package com.ruoyi.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.api.dto.ActivityDetailDto;
import com.ruoyi.api.dto.ActivityDto;
import com.ruoyi.api.mapper.MiniActivityMapper;
import com.ruoyi.api.service.IMiniActivityService;
import com.ruoyi.common.constant.Constants;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class MiniActivityServiceImpl extends ServiceImpl<MiniActivityMapper, ActivityDto> implements IMiniActivityService {

    @Override
    public List<ActivityDto> queryActivityList(Long userId) {
        List<ActivityDto> activityDtos = baseMapper.queryActivityList(userId);
        if (!io.jsonwebtoken.lang.Collections.isEmpty(activityDtos)) {
            activityDtos.forEach(activityDto -> {
                activityDto.setPicture(Constants.IMG_PREFIX + activityDto.getPicture());
            });
        }
        return activityDtos;
    }

    @Override
    public ActivityDetailDto queryActivityDetailList(Long activityId, Long userId) {
		ActivityDetailDto activityDetailDto;
		// 获取活动详细信息
        activityDetailDto = baseMapper.queryActivityDetailList(activityId);
        // 设置社团头像图片地址
        activityDetailDto.setClubAvatar(Constants.IMG_PREFIX + activityDetailDto.getClubAvatar());
        // 设置活动宣传图片地址
        activityDetailDto.setActivityPicture(Constants.IMG_PREFIX + activityDetailDto.getActivityPicture());
		// 判断用户是否已关注活动
		LambdaQueryWrapper<ActivityDto> queryWrapper = Wrappers.lambdaQuery();
		queryWrapper.eq(ActivityDto::getActivityId, activityId)
				.eq(ActivityDto::getUserId, userId);
		activityDetailDto.setIsFollow(baseMapper.selectCount(queryWrapper) > 0);
        return activityDetailDto;
    }

	@Override
	public Integer doFollow(Long userId, Long activityId, Boolean operation) {
		int num = 0;
		if (operation != null && !operation) {
			num = baseMapper.insert(new ActivityDto(userId, activityId));
		}
		if (operation != null && operation) {
			HashMap<String, Object> condMap = new HashMap<>();
			condMap.put("USER_ID", userId);
			condMap.put("ACTIVITY_ID", activityId);
			num = baseMapper.deleteByMap(condMap);
		}
		return num;
	}

	@Override
	public List<ActivityDto> maxTimesActivityList() {
		List<ActivityDto> activityDtos = baseMapper.queryMaxTimesActivityList();
		if (!io.jsonwebtoken.lang.Collections.isEmpty(activityDtos)) {
			activityDtos.forEach(activityDto -> {
				activityDto.setPicture(Constants.IMG_PREFIX+activityDto.getPicture());
			});
		}
		return activityDtos;
	}

	@Override
	public List<ActivityDto> getClubActivitys(Long userId) {
    	List<ActivityDto> activitys = new ArrayList<>();
    	// 获取关注活动信息
		activitys.addAll(baseMapper.queryActivityList(userId));
		// 获取关注的社团发布的活动信息
		activitys.addAll(baseMapper.queryClubActivitys(userId));
		// 去重
		List<ActivityDto> activityWithoutDuplicates = activitys.stream().filter(distinctByKey(ActivityDto::getActivityId)).collect(Collectors.toList());
		// 排序
		Collections.sort(activityWithoutDuplicates, new Comparator<ActivityDto>() {
			@Override
			public int compare(ActivityDto o1, ActivityDto o2) {
				return o2.getReleaseTime().compareTo(o1.getReleaseTime());
			}
		});
		// 设置图片地址
		if (!io.jsonwebtoken.lang.Collections.isEmpty(activityWithoutDuplicates)) {
			activityWithoutDuplicates.forEach(activityDto -> {
				activityDto.setPicture(Constants.IMG_PREFIX + activityDto.getPicture());
			});
		}

		return activityWithoutDuplicates;
	}

	private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

}
