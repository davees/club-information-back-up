package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Date 2021/3/31 16:32
 * @Created mobian
 * @Description 活动的类型
 */
public class ActivityEnum {

	public enum ActivityTypeEnum {

		EDU("0","教育"),
		PLAY("1","娱乐"),
		FRIENDSHIP("2","友情");


		private String name;
		private String value;

		private ActivityTypeEnum(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		// 将数据缓存到map中
		private static final Map<String, String> map = new HashMap<String, String>();

		static {
			for (ActivityTypeEnum status : ActivityTypeEnum.values()) {
				map.put(status.getName(), status.getValue());
			}
		}

		// 根据name查询value值
		public static String getValueByName(String name) {
			return map.get(name);
		}
	}

	/**
	 * 活动状态
	 */
	public enum ActivityStateEnum {

		INIT("待提交", 0),
		WAIT("待审核", 5),
		FAILED("审核失败", 10),
		SUCCED("审核成功", 20);


		private String name;
		private Integer value;

		ActivityStateEnum(String name, Integer value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public Integer getValue() {
			return value;
		}
	}
}



