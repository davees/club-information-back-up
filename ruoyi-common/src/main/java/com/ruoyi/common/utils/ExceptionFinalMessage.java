package com.ruoyi.common.utils;

/**
 * @Date 2021/3/28 13:47
 * @Created mobian
 * @Description
 */
public class ExceptionFinalMessage {

	public static final String DATA_IS_NULL = "查询的数据不存在";

	public static final String DATA_IS_ERROR = "查询的数据有误";

	public static final String RECV_MSG_IS_NULL = "接收到的数据为空";

	public static final String BUILD_APPROVAL_ERROR = "构建审批对象失败";

	public static final String STATUS_IS_APPROVAL_SUCCESS = "状态已为审批成功，不允许重复操作";

	public static final String STATUS_IS_NOT_ALLOW_DELETED = "状态已改变，不允许删除";

}
